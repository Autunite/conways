conways


Written about 1.5 years ago in verilog. Originally was a VHDL project but I switched to verilog 
because I had to use it for work.

Uses an 8x8 multiplexed LED display to display the glider moving around.
Several premade patterns exist and can all be selected using buttons:
2x2, repeater, glider

Future expansions planned are:
Move to lattice developer environment.
Add in capability to create patterns on the fly
using buttons.
Put in the r-pentomino to see what happens.

Video:
https://youtu.be/55seAFlnI6o