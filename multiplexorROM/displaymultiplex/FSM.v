`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:29:42 08/22/2017 
// Design Name: 
// Module Name:    FSM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FSM(
    input load,
    input clk,
    output reg [2:0] cAddr =0,
    output reg [2:0] rAddr =0,
    output reg en =0,
    output reg LDO =0
    );
reg [1:0] state =0;
reg [3:0] count = 0;

always@(posedge clk)
begin
	if(load ==1)
	begin
		LDO<=1;
		cAddr<=3'b000;
		rAddr<=3'b000;
		en<=0;
		state<=0;
		count <= 0;
	end
	else
	begin
		case(state)
		
		0: begin
		LDO<=0;
		en<=1;
		state<=state+1;
		end
		
		1: begin
		en<=0;
		state<=state+1;
		end
		
		2: begin
		cAddr<=cAddr+1;
		rAddr<=rAddr+1;
		state<=state+1;
		end
		
		3: begin
		//en<=1;
		//state<=0;
		//attempted modification for brighter LED's
		//
		if( count == 4'b1111)
		begin
			state <=0;
			en<=1;
		end
		else
		begin
			en<=1;
			count<= count+1;
		end
		//
		
		end
		endcase
	end
end

endmodule
