`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:40:00 09/19/2017 
// Design Name: 
// Module Name:    addrROM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module addrROM(
    input [1:0] addr,
	 output reg [7:0] row0=0,
    output reg [7:0] row1=0,
    output reg [7:0] row2=0,
    output reg [7:0] row3=0,
    output reg [7:0] row4=0,
    output reg [7:0] row5=0,
    output reg [7:0] row6=0,
    output reg [7:0] row7=0
    );

always@(addr)
begin
	case(addr)
		0:begin
			row0 <= 8'b00000000;
			row1 <= 8'b01100000;
			row2 <= 8'b01100000;
			row3 <= 8'b00000000;
			row4 <= 8'b00000000;
			row5 <= 8'b00000000;
			row6 <= 8'b00000000;
			row7 <= 8'b00000000;
		end
		
		1:begin
			row0 <= 8'b00000000;
			row1 <= 8'b00001000;
			row2 <= 8'b00001000;
			row3 <= 8'b00001000;
			row4 <= 8'b00000000;
			row5 <= 8'b00000000;
			row6 <= 8'b00000000;
			row7 <= 8'b00000000;
		end
		
		2:begin
			row0 <= 8'b10000001;
			row1 <= 8'b00000000;
			row2 <= 8'b00000000;
			row3 <= 8'b00000000;
			row4 <= 8'b00000000;
			row5 <= 8'b00000000;
			row6 <= 8'b00000000;
			row7 <= 8'b10000001;
		end
			
		3:begin
			row0 <= 8'b01000000;
			row1 <= 8'b00100000;
			row2 <= 8'b11100000;
			row3 <= 8'b00000000;
			row4 <= 8'b00000000;
			row5 <= 8'b00000000;
			row6 <= 8'b00000000;
			row7 <= 8'b00000000;
		end
	endcase
end

endmodule
