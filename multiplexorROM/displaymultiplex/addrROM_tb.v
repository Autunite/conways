`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:47:44 09/19/2017
// Design Name:   addrROM
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/addrROM_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: addrROM
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module addrROM_tb;

	// Inputs
	reg [1:0] addr;

	// Outputs
	wire [7:0] row0;
	wire [7:0] row1;
	wire [7:0] row2;
	wire [7:0] row3;
	wire [7:0] row4;
	wire [7:0] row5;
	wire [7:0] row6;
	wire [7:0] row7;

	// Instantiate the Unit Under Test (UUT)
	addrROM uut (
		.addr(addr), 
		.row0(row0), 
		.row1(row1), 
		.row2(row2), 
		.row3(row3), 
		.row4(row4), 
		.row5(row5), 
		.row6(row6), 
		.row7(row7)
	);

	initial begin
		// Initialize Inputs
		addr = 0;

		// Wait 100 ns for global reset to finish
		#100;
       addr = 1;
		 #5;
		 
		 addr = 2;
		 
		 #5;
		 
		 addr = 3;
		 
		 #5;
		 
		 addr = 0;
		// Add stimulus here

	end
      
endmodule

