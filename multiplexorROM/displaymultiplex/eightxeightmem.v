`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:26:32 08/22/2017 
// Design Name: 
// Module Name:    eightxeightmem 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module eightxeightmem(
    input [7:0] row0,
    input [7:0] row1,
    input [7:0] row2,
    input [7:0] row3,
    input [7:0] row4,
    input [7:0] row5,
    input [7:0] row6,
    input [7:0] row7,
    input Load,
    input [2:0] addr,
    output reg [7:0] out =0
    );

	reg [7:0] mem [7:0];
	
	always@(Load, addr)
	begin
		if(Load==1)
		begin
			mem[0]<=row0;
			mem[1]<=row1;
			mem[2]<=row2;
			mem[3]<=row3;
			mem[4]<=row4;
			mem[5]<=row5;
			mem[6]<=row6;
			mem[7]<=row7;
			out<=0;
		end
		
		else
		begin
			out<=mem[addr];
		end
	end

endmodule
