`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:39:47 08/23/2017
// Design Name:   enmultiplex
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/enmultiplex_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: enmultiplex
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module enmultiplex_tb;

	// Inputs
	reg [2:0] addr;
	reg en;

	// Outputs
	wire [7:0] row;

	// Instantiate the Unit Under Test (UUT)
	enmultiplex uut (
		.addr(addr), 
		.en(en), 
		.row(row)
	);

	initial begin
		// Initialize Inputs
		addr = 0;
		en = 0;

		// Wait 100 ns for global reset to finish
		#100;
      en=1;
		#1;
		addr=1;
		#1;
		addr=2;
		#1;
		addr=3;
		#1;
		addr=4;
		#1;
		addr=5;
		#1;
		addr=6;
		#1;
		addr=7;
		#1;
		en=0;
		#1;
		// Add stimulus here

	end
      
endmodule

