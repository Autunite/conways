/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000000448477695_4257913759_init();
    work_m_00000000000226162334_4020927633_init();
    work_m_00000000003966221938_2687629492_init();
    work_m_00000000002811749956_2046179431_init();
    work_m_00000000002076366957_0091589992_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000002076366957_0091589992");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
