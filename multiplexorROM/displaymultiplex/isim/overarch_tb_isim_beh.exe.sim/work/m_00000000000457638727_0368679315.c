/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/singleCell.v";
static int ng1[] = {1, 0};
static int ng2[] = {0, 0};
static int ng3[] = {2, 0};
static int ng4[] = {3, 0};
static int ng5[] = {4, 0};
static int ng6[] = {5, 0};
static int ng7[] = {6, 0};
static int ng8[] = {7, 0};



static void Always_35_0(char *t0)
{
    char t6[8];
    char t30[8];
    char t31[8];
    char t34[8];
    char t38[8];
    char t41[8];
    char t45[8];
    char t48[8];
    char t52[8];
    char t55[8];
    char t59[8];
    char t62[8];
    char t66[8];
    char t69[8];
    char t73[8];
    char t75[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    char *t32;
    char *t33;
    char *t35;
    char *t36;
    char *t37;
    char *t39;
    char *t40;
    char *t42;
    char *t43;
    char *t44;
    char *t46;
    char *t47;
    char *t49;
    char *t50;
    char *t51;
    char *t53;
    char *t54;
    char *t56;
    char *t57;
    char *t58;
    char *t60;
    char *t61;
    char *t63;
    char *t64;
    char *t65;
    char *t67;
    char *t68;
    char *t70;
    char *t71;
    char *t72;
    char *t74;
    char *t76;
    char *t77;
    char *t78;
    char *t79;
    char *t80;
    char *t81;

LAB0:    t1 = (t0 + 2848U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(35, ng0);
    t2 = (t0 + 3168);
    *((int *)t2) = 1;
    t3 = (t0 + 2880);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(36, ng0);

LAB5:    xsi_set_current_line(37, ng0);
    t4 = (t0 + 1528U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(42, ng0);

LAB14:    xsi_set_current_line(47, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    t2 = (t0 + 1008U);
    t4 = (t2 + 72U);
    t5 = *((char **)t4);
    t7 = ((char*)((ng2)));
    xsi_vlog_generic_get_index_select_value(t6, 32, t3, t5, 2, t7, 32, 1);
    t8 = (t0 + 1048U);
    t21 = *((char **)t8);
    t8 = (t0 + 1008U);
    t22 = (t8 + 72U);
    t28 = *((char **)t22);
    t29 = ((char*)((ng1)));
    xsi_vlog_generic_get_index_select_value(t30, 32, t21, t28, 2, t29, 32, 1);
    memset(t31, 0, 8);
    xsi_vlog_unsigned_add(t31, 32, t6, 32, t30, 32);
    t32 = (t0 + 1048U);
    t33 = *((char **)t32);
    t32 = (t0 + 1008U);
    t35 = (t32 + 72U);
    t36 = *((char **)t35);
    t37 = ((char*)((ng3)));
    xsi_vlog_generic_get_index_select_value(t34, 32, t33, t36, 2, t37, 32, 1);
    memset(t38, 0, 8);
    xsi_vlog_unsigned_add(t38, 32, t31, 32, t34, 32);
    t39 = (t0 + 1048U);
    t40 = *((char **)t39);
    t39 = (t0 + 1008U);
    t42 = (t39 + 72U);
    t43 = *((char **)t42);
    t44 = ((char*)((ng4)));
    xsi_vlog_generic_get_index_select_value(t41, 32, t40, t43, 2, t44, 32, 1);
    memset(t45, 0, 8);
    xsi_vlog_unsigned_add(t45, 32, t38, 32, t41, 32);
    t46 = (t0 + 1048U);
    t47 = *((char **)t46);
    t46 = (t0 + 1008U);
    t49 = (t46 + 72U);
    t50 = *((char **)t49);
    t51 = ((char*)((ng5)));
    xsi_vlog_generic_get_index_select_value(t48, 32, t47, t50, 2, t51, 32, 1);
    memset(t52, 0, 8);
    xsi_vlog_unsigned_add(t52, 32, t45, 32, t48, 32);
    t53 = (t0 + 1048U);
    t54 = *((char **)t53);
    t53 = (t0 + 1008U);
    t56 = (t53 + 72U);
    t57 = *((char **)t56);
    t58 = ((char*)((ng6)));
    xsi_vlog_generic_get_index_select_value(t55, 32, t54, t57, 2, t58, 32, 1);
    memset(t59, 0, 8);
    xsi_vlog_unsigned_add(t59, 32, t52, 32, t55, 32);
    t60 = (t0 + 1048U);
    t61 = *((char **)t60);
    t60 = (t0 + 1008U);
    t63 = (t60 + 72U);
    t64 = *((char **)t63);
    t65 = ((char*)((ng7)));
    xsi_vlog_generic_get_index_select_value(t62, 32, t61, t64, 2, t65, 32, 1);
    memset(t66, 0, 8);
    xsi_vlog_unsigned_add(t66, 32, t59, 32, t62, 32);
    t67 = (t0 + 1048U);
    t68 = *((char **)t67);
    t67 = (t0 + 1008U);
    t70 = (t67 + 72U);
    t71 = *((char **)t70);
    t72 = ((char*)((ng8)));
    xsi_vlog_generic_get_index_select_value(t69, 32, t68, t71, 2, t72, 32, 1);
    memset(t73, 0, 8);
    xsi_vlog_unsigned_add(t73, 32, t66, 32, t69, 32);
    t74 = ((char*)((ng3)));
    memset(t75, 0, 8);
    t76 = (t73 + 4);
    if (*((unsigned int *)t76) != 0)
        goto LAB16;

LAB15:    t77 = (t74 + 4);
    if (*((unsigned int *)t77) != 0)
        goto LAB16;

LAB19:    if (*((unsigned int *)t73) < *((unsigned int *)t74))
        goto LAB17;

LAB18:    t79 = (t75 + 4);
    t9 = *((unsigned int *)t79);
    t10 = (~(t9));
    t11 = *((unsigned int *)t75);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB20;

LAB21:
LAB22:    xsi_set_current_line(51, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    t2 = (t0 + 1008U);
    t4 = (t2 + 72U);
    t5 = *((char **)t4);
    t7 = ((char*)((ng2)));
    xsi_vlog_generic_get_index_select_value(t6, 32, t3, t5, 2, t7, 32, 1);
    t8 = (t0 + 1048U);
    t21 = *((char **)t8);
    t8 = (t0 + 1008U);
    t22 = (t8 + 72U);
    t28 = *((char **)t22);
    t29 = ((char*)((ng1)));
    xsi_vlog_generic_get_index_select_value(t30, 32, t21, t28, 2, t29, 32, 1);
    memset(t31, 0, 8);
    xsi_vlog_unsigned_add(t31, 32, t6, 32, t30, 32);
    t32 = (t0 + 1048U);
    t33 = *((char **)t32);
    t32 = (t0 + 1008U);
    t35 = (t32 + 72U);
    t36 = *((char **)t35);
    t37 = ((char*)((ng3)));
    xsi_vlog_generic_get_index_select_value(t34, 32, t33, t36, 2, t37, 32, 1);
    memset(t38, 0, 8);
    xsi_vlog_unsigned_add(t38, 32, t31, 32, t34, 32);
    t39 = (t0 + 1048U);
    t40 = *((char **)t39);
    t39 = (t0 + 1008U);
    t42 = (t39 + 72U);
    t43 = *((char **)t42);
    t44 = ((char*)((ng4)));
    xsi_vlog_generic_get_index_select_value(t41, 32, t40, t43, 2, t44, 32, 1);
    memset(t45, 0, 8);
    xsi_vlog_unsigned_add(t45, 32, t38, 32, t41, 32);
    t46 = (t0 + 1048U);
    t47 = *((char **)t46);
    t46 = (t0 + 1008U);
    t49 = (t46 + 72U);
    t50 = *((char **)t49);
    t51 = ((char*)((ng5)));
    xsi_vlog_generic_get_index_select_value(t48, 32, t47, t50, 2, t51, 32, 1);
    memset(t52, 0, 8);
    xsi_vlog_unsigned_add(t52, 32, t45, 32, t48, 32);
    t53 = (t0 + 1048U);
    t54 = *((char **)t53);
    t53 = (t0 + 1008U);
    t56 = (t53 + 72U);
    t57 = *((char **)t56);
    t58 = ((char*)((ng6)));
    xsi_vlog_generic_get_index_select_value(t55, 32, t54, t57, 2, t58, 32, 1);
    memset(t59, 0, 8);
    xsi_vlog_unsigned_add(t59, 32, t52, 32, t55, 32);
    t60 = (t0 + 1048U);
    t61 = *((char **)t60);
    t60 = (t0 + 1008U);
    t63 = (t60 + 72U);
    t64 = *((char **)t63);
    t65 = ((char*)((ng7)));
    xsi_vlog_generic_get_index_select_value(t62, 32, t61, t64, 2, t65, 32, 1);
    memset(t66, 0, 8);
    xsi_vlog_unsigned_add(t66, 32, t59, 32, t62, 32);
    t67 = (t0 + 1048U);
    t68 = *((char **)t67);
    t67 = (t0 + 1008U);
    t70 = (t67 + 72U);
    t71 = *((char **)t70);
    t72 = ((char*)((ng8)));
    xsi_vlog_generic_get_index_select_value(t69, 32, t68, t71, 2, t72, 32, 1);
    memset(t73, 0, 8);
    xsi_vlog_unsigned_add(t73, 32, t66, 32, t69, 32);
    t74 = ((char*)((ng4)));
    memset(t75, 0, 8);
    t76 = (t73 + 4);
    t77 = (t74 + 4);
    t9 = *((unsigned int *)t73);
    t10 = *((unsigned int *)t74);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t76);
    t13 = *((unsigned int *)t77);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t76);
    t17 = *((unsigned int *)t77);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB27;

LAB24:    if (t18 != 0)
        goto LAB26;

LAB25:    *((unsigned int *)t75) = 1;

LAB27:    t79 = (t75 + 4);
    t23 = *((unsigned int *)t79);
    t24 = (~(t23));
    t25 = *((unsigned int *)t75);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB28;

LAB29:
LAB30:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    t2 = (t0 + 1008U);
    t4 = (t2 + 72U);
    t5 = *((char **)t4);
    t7 = ((char*)((ng2)));
    xsi_vlog_generic_get_index_select_value(t6, 32, t3, t5, 2, t7, 32, 1);
    t8 = (t0 + 1048U);
    t21 = *((char **)t8);
    t8 = (t0 + 1008U);
    t22 = (t8 + 72U);
    t28 = *((char **)t22);
    t29 = ((char*)((ng1)));
    xsi_vlog_generic_get_index_select_value(t30, 32, t21, t28, 2, t29, 32, 1);
    memset(t31, 0, 8);
    xsi_vlog_unsigned_add(t31, 32, t6, 32, t30, 32);
    t32 = (t0 + 1048U);
    t33 = *((char **)t32);
    t32 = (t0 + 1008U);
    t35 = (t32 + 72U);
    t36 = *((char **)t35);
    t37 = ((char*)((ng3)));
    xsi_vlog_generic_get_index_select_value(t34, 32, t33, t36, 2, t37, 32, 1);
    memset(t38, 0, 8);
    xsi_vlog_unsigned_add(t38, 32, t31, 32, t34, 32);
    t39 = (t0 + 1048U);
    t40 = *((char **)t39);
    t39 = (t0 + 1008U);
    t42 = (t39 + 72U);
    t43 = *((char **)t42);
    t44 = ((char*)((ng4)));
    xsi_vlog_generic_get_index_select_value(t41, 32, t40, t43, 2, t44, 32, 1);
    memset(t45, 0, 8);
    xsi_vlog_unsigned_add(t45, 32, t38, 32, t41, 32);
    t46 = (t0 + 1048U);
    t47 = *((char **)t46);
    t46 = (t0 + 1008U);
    t49 = (t46 + 72U);
    t50 = *((char **)t49);
    t51 = ((char*)((ng5)));
    xsi_vlog_generic_get_index_select_value(t48, 32, t47, t50, 2, t51, 32, 1);
    memset(t52, 0, 8);
    xsi_vlog_unsigned_add(t52, 32, t45, 32, t48, 32);
    t53 = (t0 + 1048U);
    t54 = *((char **)t53);
    t53 = (t0 + 1008U);
    t56 = (t53 + 72U);
    t57 = *((char **)t56);
    t58 = ((char*)((ng6)));
    xsi_vlog_generic_get_index_select_value(t55, 32, t54, t57, 2, t58, 32, 1);
    memset(t59, 0, 8);
    xsi_vlog_unsigned_add(t59, 32, t52, 32, t55, 32);
    t60 = (t0 + 1048U);
    t61 = *((char **)t60);
    t60 = (t0 + 1008U);
    t63 = (t60 + 72U);
    t64 = *((char **)t63);
    t65 = ((char*)((ng7)));
    xsi_vlog_generic_get_index_select_value(t62, 32, t61, t64, 2, t65, 32, 1);
    memset(t66, 0, 8);
    xsi_vlog_unsigned_add(t66, 32, t59, 32, t62, 32);
    t67 = (t0 + 1048U);
    t68 = *((char **)t67);
    t67 = (t0 + 1008U);
    t70 = (t67 + 72U);
    t71 = *((char **)t70);
    t72 = ((char*)((ng8)));
    xsi_vlog_generic_get_index_select_value(t69, 32, t68, t71, 2, t72, 32, 1);
    memset(t73, 0, 8);
    xsi_vlog_unsigned_add(t73, 32, t66, 32, t69, 32);
    t74 = ((char*)((ng4)));
    memset(t75, 0, 8);
    t76 = (t73 + 4);
    if (*((unsigned int *)t76) != 0)
        goto LAB33;

LAB32:    t77 = (t74 + 4);
    if (*((unsigned int *)t77) != 0)
        goto LAB33;

LAB36:    if (*((unsigned int *)t73) > *((unsigned int *)t74))
        goto LAB34;

LAB35:    t79 = (t75 + 4);
    t9 = *((unsigned int *)t79);
    t10 = (~(t9));
    t11 = *((unsigned int *)t75);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB37;

LAB38:
LAB39:
LAB12:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(38, ng0);

LAB13:    xsi_set_current_line(39, ng0);
    t28 = (t0 + 1368U);
    t29 = *((char **)t28);
    t28 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t28, t29, 0, 0, 1, 0LL);
    goto LAB12;

LAB16:    t78 = (t75 + 4);
    *((unsigned int *)t75) = 1;
    *((unsigned int *)t78) = 1;
    goto LAB18;

LAB17:    *((unsigned int *)t75) = 1;
    goto LAB18;

LAB20:    xsi_set_current_line(48, ng0);

LAB23:    xsi_set_current_line(49, ng0);
    t80 = ((char*)((ng2)));
    t81 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t81, t80, 0, 0, 1, 0LL);
    goto LAB22;

LAB26:    t78 = (t75 + 4);
    *((unsigned int *)t75) = 1;
    *((unsigned int *)t78) = 1;
    goto LAB27;

LAB28:    xsi_set_current_line(52, ng0);

LAB31:    xsi_set_current_line(53, ng0);
    t80 = ((char*)((ng1)));
    t81 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t81, t80, 0, 0, 1, 0LL);
    goto LAB30;

LAB33:    t78 = (t75 + 4);
    *((unsigned int *)t75) = 1;
    *((unsigned int *)t78) = 1;
    goto LAB35;

LAB34:    *((unsigned int *)t75) = 1;
    goto LAB35;

LAB37:    xsi_set_current_line(56, ng0);

LAB40:    xsi_set_current_line(57, ng0);
    t80 = ((char*)((ng2)));
    t81 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t81, t80, 0, 0, 1, 0LL);
    goto LAB39;

}


extern void work_m_00000000000457638727_0368679315_init()
{
	static char *pe[] = {(void *)Always_35_0};
	xsi_register_didat("work_m_00000000000457638727_0368679315", "isim/overarch_tb_isim_beh.exe.sim/work/m_00000000000457638727_0368679315.didat");
	xsi_register_executes(pe);
}
