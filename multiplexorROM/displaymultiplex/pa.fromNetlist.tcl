
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name displaymultiplex -dir "C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/planAhead_run_3" -part xc3s500efg320-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/overarch.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "overarch.ucf" [current_fileset -constrset]
add_files [list {overarch.ucf}] -fileset [get_property constrset [current_run]]
link_design
