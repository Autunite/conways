`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:58:23 09/19/2017
// Design Name:   toroidalwrapper
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/toroid_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: toroidalwrapper
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module toroid_tb;

	// Inputs
	reg clk;
	reg load;
	reg [7:0] in0;
	reg [7:0] in1;
	reg [7:0] in2;
	reg [7:0] in3;
	reg [7:0] in4;
	reg [7:0] in5;
	reg [7:0] in6;
	reg [7:0] in7;

	// Outputs
	wire [7:0] out0;
	wire [7:0] out1;
	wire [7:0] out2;
	wire [7:0] out3;
	wire [7:0] out4;
	wire [7:0] out5;
	wire [7:0] out6;
	wire [7:0] out7;

	// Instantiate the Unit Under Test (UUT)
	toroidalwrapper uut (
		.clk(clk), 
		.load(load), 
		.in0(in0), 
		.in1(in1), 
		.in2(in2), 
		.in3(in3), 
		.in4(in4), 
		.in5(in5), 
		.in6(in6), 
		.in7(in7), 
		.out0(out0), 
		.out1(out1), 
		.out2(out2), 
		.out3(out3), 
		.out4(out4), 
		.out5(out5), 
		.out6(out6), 
		.out7(out7)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		load = 0;
		in0 = 0;
		in1 = 0;
		in2 = 0;
		in3 = 0;
		in4 = 0;
		in5 = 0;
		in6 = 0;
		in7 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		//corner block
		//neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 8'b10000001;
		in1 = 8'b00000000;
		in2 = 8'b00000000;
		in3 = 8'b00000000;
		in4 = 8'b00000000;
		in5 = 8'b00000000;
		in6 = 8'b00000000;
		in7 = 8'b10000001;

		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		load = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		//glider
		//neighbors = 0;
		clk = 0;
		load = 0;
		in0 = 8'b01000000;
		in1 = 8'b00100000;
		in2 = 8'b11100000;
		in3 = 8'b00000000;
		in4 = 8'b00000000;
		in5 = 8'b00000000;
		in6 = 8'b00000000;
		in7 = 8'b00000000;

		load = 1;
		#5;
		clk = 1;
		#5;
		clk = 0;
		load = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		#5;
		clk =1;
		#5;
		clk = 0;
		
		
	end
      
endmodule

