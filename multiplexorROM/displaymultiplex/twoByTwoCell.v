`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:37:59 08/28/2017 
// Design Name: 
// Module Name:    twoByTwoCell 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//		This unit combines four cells to make a 2x2 cell matrix
//////////////////////////////////////////////////////////////////////////////////
module twoByTwoCell(
    input [19:0] neighbor,
    input clk,
    input load,
    input [1:0] in0,
    input [1:0] in1,
    output [1:0] out0,
    output [1:0] out1
    );

wire [7:0] cell00Neighbor;
wire [7:0] cell10Neighbor;
wire [7:0] cell01Neighbor;
wire [7:0] cell11Neighbor;

//upper left cell
singleCell c00(
    .neighbor(cell00Neighbor),
    .clk(clk),
	 .loadInput(in0[1]),
    .load(load),
    .out(out0[1])
    );

//upper right cell
singleCell c10(
    .neighbor(cell10Neighbor),
    .clk(clk),
	 .loadInput(in0[0]),
    .load(load),
    .out(out0[0])
    );

//bottom left cell	 
singleCell c01(
    .neighbor(cell01Neighbor),
    .clk(clk),
	 .loadInput(in1[1]),
    .load(load),
    .out(out1[1])
    );

//bottom right cell 
singleCell c11(
    .neighbor(cell11Neighbor),
    .clk(clk),
	 .loadInput(in1[0]),
    .load(load),
    .out(out1[0])
    );

//assign neighbors to exterior ports of two by two cell
//upper left
assign cell00Neighbor = {neighbor[19],neighbor[18],out0[0],out1[0],out1[1],neighbor[2],neighbor[1],neighbor[0]};

//upper right
assign cell10Neighbor = {neighbor[16],neighbor[15],neighbor[14],neighbor[13],out1[0],out1[1],out0[1],neighbor[17]};

//bottom left
assign cell01Neighbor = {out0[1],out0[0],out1[0],neighbor[7],neighbor[6],neighbor[5],neighbor[4],neighbor[3]};

//bottom right
assign cell11Neighbor = {out0[0],neighbor[12],neighbor[11],neighbor[10],neighbor[9],neighbor[8],out1[1],out0[1]};


endmodule
